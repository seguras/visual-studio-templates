﻿namespace NugetTemplate
{
	using System;

#pragma warning disable CA1032 // Implement standard exception constructors
	/// <summary>
	/// Levée lorsqu'aucun utilisateur ne correspond à un identifiant de connexion.
	/// </summary>
	public class UserNotFoundException : Exception
#pragma warning restore CA1032 // Implement standard exception constructors
	{
		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="UserNotFoundException"/>.
		/// </summary>
		/// <param name="userName">Identifiant de connexion de l'utilisateur.</param>
		/// <param name="cause">Cause de l'erreur.</param>
		public UserNotFoundException(string userName, string cause)
			: base($"Impossible de trouver l'utilisateur correspondant à l'identifiant de connexion '{userName}' : {cause}.")
		{
		}
	}
}