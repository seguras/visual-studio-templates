@echo off
set nupkgVersion=%1
set nupkgExtension=.nupkg
set symbolsnupkgExtension=.symbols.nupkg

IF [%1] == [] GOTO ERROR

::Déclaration des variables du nuget NugetTemplate
set nupkgFolder=..\bin\Release\NugetTemplate.
set nupkgPath=%nupkgFolder%%nupkgVersion%%nupkgExtension%
set symbolsnupkgPath=%nupkgFolder%%nupkgVersion%%nupkgExtension%

@echo ----------------------------------------------------------
@echo Copie du package Nuget NugetTemplate
@echo on
copy %nupkgPath%  \\srv-dev-01\Packages
@echo off
@echo ----------------------------------------------------------
@echo Copie des symbols du package Nuget NugetTemplate
@echo on
copy %symbolsnupkgPath%  \\srv-dev-01\Symbols
@echo off


goto :eof
:ERROR
echo Veuillez specifier la version en parametre