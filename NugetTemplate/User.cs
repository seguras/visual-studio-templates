﻿namespace NugetTemplate
{
	/// <summary>
	/// Représente un utilisateur authentifié.
	/// </summary>
	public class User
	{
		/// <summary>
		/// Obtient ou définit l'id de l'utilisateur.
		/// </summary>
		public string Id { get; set; }

		/// <summary>
		/// Obtient ou définit l'id du client auquel appartient l'utilisateur.
		/// </summary>
		public string ClientId { get; set; }

		/// <summary>
		/// Obtient ou définit le nom de l'utilisateur.
		/// </summary>
		public string LastName { get; set; }

		/// <summary>
		/// Obtient ou définit le prénom de l'utilisateur.
		/// </summary>
		public string FirstName { get; set; }

		/// <summary>
		/// Obtient ou définit l'adresse e-mail de l'utilisateur.
		/// </summary>
		public string Email { get; set; }

		/// <summary>
		/// Obtient ou définit le mot de passe de l'utilisateur.
		/// </summary>
		public string Password { get; set; }
	}
}