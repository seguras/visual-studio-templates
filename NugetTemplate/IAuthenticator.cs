﻿namespace NugetTemplate
{
	using System.Collections.Generic;

	/// <summary>
	/// Définie l'interface necessaire pour implémenter un module d'authentification.
	/// </summary>
	public interface IAuthenticator
	{
		/// <summary>
		/// Récupère l'utilisateur correspondant à un identifiant de connexion.
		/// </summary>
		/// <param name="userName">Identifiant de connexion.</param>
		/// <returns>Instance de User.</returns>
		/// <exception cref="UserNotFoundException">Si aucun utilisateur ne correspond à l'identifiant de connexion.</exception>
		User GetUser(string userName);

		/// <summary>
		/// Retourne une valeur indiquant si un utilisateur est authentifié.
		/// </summary>
		/// <param name="user">Instance de User.</param>
		/// <param name="password">Mot de passe saisi par l'utilisateur.</param>
		/// <returns>True si l'utilisateur est authentifié; false autrement.</returns>
		bool IsAuthenticated(User user, string password);

		/// <summary>
		/// Retourne la liste des roles d'un utilisateur.
		/// </summary>
		/// <param name="userId">Id de l'utilisateur.</param>
		/// <returns>La liste des roles de l'utilisateur.</returns>
		IEnumerable<string> GetRoles(string userId);
	}
}