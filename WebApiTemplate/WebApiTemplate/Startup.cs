﻿namespace WebApiTemplate
{
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.IO;
	using System.Threading.Tasks;
	using Avm.Logging.Extensions;
	using Avm.Logging.NLog.AspNetCore.Extensions;
	using Avm.Logging.NLogAdapter;
	using AvmUp.Authentication;
	using Microsoft.AspNetCore.Authentication.JwtBearer;
	using Microsoft.AspNetCore.Builder;
	using Microsoft.AspNetCore.Hosting;
	using Microsoft.AspNetCore.Mvc;
	using Microsoft.Extensions.Configuration;
	using Microsoft.Extensions.DependencyInjection;
	using Microsoft.Extensions.Logging;
	using Microsoft.OpenApi.Models;
	using NLog.Web;
	using WebApiTemplate.Attributes;
	using WebApiTemplate.Services;
	using WebApiTemplate.Services.Implementations;
	using WebApiTemplate.Settings;

	/// <summary>
	/// Démarrage de la Web Api.
	/// </summary>
	public class Startup
	{
		/// <summary>
		/// Logger de l'application.
		/// </summary>
		private static NLogAdapter<Program> logger = new NLogAdapter<Program>();

		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="Startup"/>.
		/// </summary>
		/// <param name="env">IHostingEnvironment env.</param>
		public Startup(IHostingEnvironment env)
		{
			env.ConfigureNLog("NLog.config");
			env.LoggingStartup();

			var config = new ConfigurationBuilder()
				 .SetBasePath(Directory.GetCurrentDirectory())
				 .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
				 .AddJsonFile("hosting.json", optional: true, reloadOnChange: true)
				 .AddEnvironmentVariables();

			this.Configuration = config.Build();
		}

		/// <summary>
		/// Obtient la configuration de la Web Api.
		/// </summary>
		public IConfiguration Configuration { get; }

		/// <summary>
		/// Configuration des services de l'application.
		/// </summary>
		/// <param name="services">Conteneur de services.</param>
		public void ConfigureServices(IServiceCollection services)
		{
			ConfigureCors(services);

			ConfigureMvc(services);

			services.AddOptions();

			ConfigureSwagger(services);

			services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
			services.Configure<AuthSettings>(Configuration.GetSection("AppSettings").GetSection("AuthSettings"));

			services.AddSingleton(typeof(Avm.Logging.Abstractions.ILogger<>), typeof(Avm.Logging.NLogAdapter.NLogAdapter<>));

			services.AddSingleton<IAuthService, AuthService>();
			services.AddSingleton<IWhiteLabelService, WhiteLabelService>();

			// Implémentez l'interface IAuthenticator
			// services.AddSingleton<IAuthenticator, ...>();

			services.AddHttpContextAccessor();

			ConfigureAuthentication(services);
		}

		/// <summary>
		/// Configure le pipeline de requête HTTP.
		/// </summary>
		/// <param name="app">Classe utilisé pour configurer le pipeline de requête HTTP de l'application.</param>
		/// <param name="env">Informations sur l'environnement hébergeant l'application.</param>
		/// <param name="loggerFactory">Instance de la fabrique de logger.</param>
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
		{
			EnableCors(app);

			// Gestion des logs(avec le nuget avm.logging)
			loggerFactory.AddLoggingProviders();

			app.AddWebLoggingServices();

			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseHsts();
			}

			app.UseHttpsRedirection();
			app.UseAuthentication();
			app.UseMvc();

			app.UseSwagger();
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebApiTemplate v1");
			});

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller=Home}/{action=Index}/{id?}");

				routes.MapSpaFallbackRoute(
					name: "spa-fallback",
					defaults: new { controller = "Home", action = "Index" });
			});
		}

		/// <summary>
		/// Active un middleeware CORS pour autoriser les requetes cross domain.
		/// </summary>
		/// <param name="app">Configuration de l'application.</param>
		/// <remarks>Uniquement en debug. Car en release, définit via le web.config.</remarks>
		[Conditional("DEBUG")]
		private static void EnableCors(IApplicationBuilder app)
		{
			app.UseCors("EnableCORS");
		}

		private static void ConfigureAuthentication(IServiceCollection services)
		{
			var serviceProvider = services.BuildServiceProvider();

			var authService = serviceProvider.GetRequiredService(typeof(IAuthService)) as IAuthService;

			services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
				.AddJwtBearer(options =>
				{
					options.TokenValidationParameters = authService.GetTokenValidationParameters();

					options.Events = new JwtBearerEvents
					{
						OnAuthenticationFailed = (context) =>
						{
							logger.LogWarning($"L'authentification a échouée : {context.Exception.Message}");

							return Task.FromResult(0);
						},
					};
				});
		}

		/// <summary>
		/// Configure le partage de ressources CROSS-ORIGIN.
		/// </summary>
		/// <param name="services">Conteneur de services.</param>
		private static void ConfigureCors(IServiceCollection services)
		{
			services.AddCors(options =>
			{
				options.AddPolicy("EnableCORS", builder =>
				{
					builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials().WithExposedHeaders("Content-Disposition").Build();
				});
			});
		}

		/// <summary>
		/// Configure MVC.
		/// </summary>
		/// <param name="services">Conteneur de services.</param>
		private static void ConfigureMvc(IServiceCollection services)
		{
			services.AddMvc(config =>
			{
				config.Filters.Add(typeof(LoggingFilterAttribute));
				config.Filters.Add(typeof(CustomExceptionFilterAttribute));
				config.Filters.Add(typeof(ModelValidationFilterAttribute));
			}).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
		}

		/// <summary>
		/// Configure Swagger.
		/// </summary>
		/// <param name="services">Conteneur de services.</param>
		private static void ConfigureSwagger(IServiceCollection services)
		{
			services.AddSwaggerGen(x =>
			{
				x.SwaggerDoc("v1", new OpenApiInfo
				{
					Title = "WebApiTemplate",
					Version = "v1",
					Description = "WebApiTemplate ASP.NET Core 2.1 Web API",
				});

				x.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "WebApiTemplate.xml"));

				x.AddSecurityDefinition(
					"Bearer",
					new OpenApiSecurityScheme
					{
						In = ParameterLocation.Header,
						Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
						Name = "Authorization",
						Type = SecuritySchemeType.ApiKey,
						Scheme = "Bearer",
					});
				x.AddSecurityRequirement(new OpenApiSecurityRequirement
				{
					{
						new OpenApiSecurityScheme
						{
							Reference = new OpenApiReference
							{
								Type = ReferenceType.SecurityScheme,
								Id = "Bearer",
							},
							Scheme = "oauth2",
							Name = "Bearer",
							In = ParameterLocation.Header,
						},
						new List<string>()
					},
				});
			});
		}
	}
}