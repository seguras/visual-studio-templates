﻿namespace WebApiTemplate.Attributes
{
	using System;
	using Avm.Logging.Abstractions;
	using Avm.Logging.Extensions;
	using Microsoft.AspNetCore.Mvc;
	using Microsoft.AspNetCore.Mvc.Filters;
	using Newtonsoft.Json;

	/// <summary>
	/// Filtre utilisé pour tracer les appels au webservice.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class LoggingFilterAttribute : ActionFilterAttribute
	{
		/// <summary>
		/// Instance du Logger.
		/// </summary>
		private readonly ILogger<LoggingFilterAttribute> logger;

		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="LoggingFilterAttribute"/>.
		/// </summary>
		/// <param name="logger">Interface du logger.</param>
		public LoggingFilterAttribute(ILogger<LoggingFilterAttribute> logger)
		{
			this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
		}

		/// <summary>
		/// Appellé avant que la méthode d'action d'un controller soit appellée.
		/// </summary>
		/// <param name="actionContext">Informations sur la requête courante et l'action.</param>
		public override void OnActionExecuting(ActionExecutingContext actionContext)
		{
			if (actionContext == null)
			{
				throw new ArgumentNullException(nameof(actionContext));
			}

			var controllerName = actionContext.RouteData.Values["controller"].ToString();
			var actionName = actionContext.RouteData.Values["action"].ToString();

			this.logger.LogInformation($"{GetSessionId(actionContext)} / {GetRequestId(actionContext)} - Url : {actionContext.HttpContext.Request.Path}");
			this.logger.LogDebug($"{GetSessionId(actionContext)} / {GetRequestId(actionContext)} - Appel du controleur : {controllerName} - Action : {actionName} - Arguments : {JsonConvert.SerializeObject(actionContext.ActionArguments)}");
		}

		/// <summary>
		/// Appellé après que la méthode d'action d'un controller soit appellée.
		/// </summary>
		/// <param name="filterContext">Informations sur le résultat de la requête courante.</param>
		public override void OnResultExecuted(ResultExecutedContext filterContext)
		{
			if (filterContext == null)
			{
				throw new ArgumentNullException(nameof(filterContext));
			}

			var result = filterContext.Result as OkObjectResult;

			if (result != null)
			{
				this.logger.LogInformation($"{GetSessionId(filterContext)} / {GetRequestId(filterContext)} - Reponse [HttpCode : {result.StatusCode} - Body : {result.Value}]");
			}
			else
			{
				var castResult = filterContext.Result as OkResult;

				if (castResult != null)
				{
					this.logger.LogInformation($"{GetSessionId(filterContext)} / {GetRequestId(filterContext)} - Reponse [HttpCode : {castResult.StatusCode}]");
				}
				else
				{
					if (filterContext.Result is NotFoundResult)
					{
						this.logger.LogInformation($"{GetSessionId(filterContext)} / {GetRequestId(filterContext)} - Reponse [HttpCode : 404]");
					}
					else if (filterContext.Result is StatusCodeResult)
					{
						this.logger.LogInformation($"{GetSessionId(filterContext)} / {GetRequestId(filterContext)} - Reponse [HttpCode : {((StatusCodeResult)filterContext.Result).StatusCode}]");
					}
				}
			}
		}

		/// <summary>
		/// Retourne l'id de session.
		/// </summary>
		/// <param name="actionContext">Contexte de l'action.</param>
		/// <returns>L'id de session.</returns>
		private string GetSessionId(ActionContext actionContext)
		{
			return actionContext.HttpContext.Request.Headers["SessionId"];
		}

		/// <summary>
		/// Retourne l'id de session.
		/// </summary>
		/// <param name="actionContext">Contexte de l'action.</param>
		/// <returns>L'id de session.</returns>
		private string GetRequestId(ActionContext actionContext)
		{
			return actionContext.HttpContext.TraceIdentifier;
		}
	}
}