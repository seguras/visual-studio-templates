﻿namespace WebApiTemplate.Attributes
{
	using System;
	using System.Linq;
	using System.Reflection;
	using Microsoft.AspNetCore.Mvc.Controllers;
	using Microsoft.AspNetCore.Mvc.Filters;
	using WebApiTemplate.Exceptions;
	using WebApiTemplate.Models;

	/// <summary>
	/// Attribut permettant de savoir si un objet peut être null.
	/// </summary>
	[AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = true)]
	public sealed class RequiredModelAttribute : Attribute
	{
		/// <summary>
		/// Vérifie si un modèle est null alors qu'il ne devrait pas.
		/// </summary>
		/// <param name="context">Context de la validation.</param>
		public static void EnsureRequiredParameterIsNotNull(ActionExecutingContext context)
		{
			if (context == null)
			{
				throw new ArgumentNullException(nameof(context));
			}

			var requiredParameters = context.ActionDescriptor.Parameters
											.Where(p => ((ControllerParameterDescriptor)p).ParameterInfo.GetCustomAttributes<RequiredModelAttribute>() != null)
											.Select(p => p.Name);

			var errors = new BadRequestError();

			foreach (var argument in context.ActionArguments.Where(a => requiredParameters.Contains(a.Key, StringComparer.Ordinal)))
			{
				if (argument.Value == null)
				{
					errors.Errors.Add(new ParameterError(argument.Key, "Le paramètre ne peut pas être null."));
				}
			}

			if (errors.Errors.Count != 0)
			{
				throw new BadRequestException(errors);
			}
		}
	}
}