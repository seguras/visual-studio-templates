﻿namespace WebApiTemplate.Attributes
{
	using System;
	using System.Linq;
	using Avm.Logging.Abstractions;
	using Avm.Logging.Extensions;
	using Microsoft.AspNetCore.Http;
	using Microsoft.AspNetCore.Mvc;
	using Microsoft.AspNetCore.Mvc.Filters;
	using Microsoft.AspNetCore.Mvc.ModelBinding;
	using WebApiTemplate.Exceptions;
	using WebApiTemplate.Models;

	/// <summary>
	/// Filtre utilisé pour valider les modèles passés aux controlleurs.
	/// </summary>
	public class ModelValidationFilterAttribute : ActionFilterAttribute
	{
		/// <summary>
		/// Instance du Logger.
		/// </summary>
		private readonly ILogger<ModelValidationFilterAttribute> logger;

		/// <summary>
		/// Permet l'accès au contexte Http.
		/// </summary>
		private readonly IHttpContextAccessor httpContextAccessor;

		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="ModelValidationFilterAttribute"/>.
		/// </summary>
		/// <param name="logger">Interface du logger.</param>
		/// <param name="httpContextAccessor">Permet l'accès au contexte Http.</param>
		public ModelValidationFilterAttribute(ILogger<ModelValidationFilterAttribute> logger, IHttpContextAccessor httpContextAccessor)
		{
			this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
			this.httpContextAccessor = httpContextAccessor ?? throw new ArgumentNullException(nameof(httpContextAccessor));
		}

		/// <summary>
		/// Obtient l'id de session.
		/// </summary>
		private string SessionId
		{
			get
			{
				return this.httpContextAccessor.HttpContext.Request.Headers["SessionId"];
			}
		}

		/// <summary>
		/// Obtient l'id de la requête.
		/// </summary>
		private string RequestId
		{
			get
			{
				return this.httpContextAccessor.HttpContext.TraceIdentifier;
			}
		}

		/// <summary>
		/// Permet la validation des models.
		/// </summary>
		/// <param name="context">Context de la validation.</param>
		public override void OnActionExecuting(ActionExecutingContext context)
		{
			if (context == null)
			{
				throw new ArgumentNullException(nameof(context));
			}

			base.OnActionExecuting(context);

			context.Result = Result(context.ModelState);
		}

		/// <summary>
		/// Valide le model.
		/// </summary>
		/// <param name="modelState">model à valider.</param>
		/// <returns>Retourne un interface.</returns>
		public virtual IActionResult Result(ModelStateDictionary modelState)
		{
			if (modelState == null)
			{
				throw new ArgumentNullException(nameof(modelState));
			}

			if (modelState.IsValid)
			{
				this.logger.LogInformation($"{SessionId} / {RequestId} - La requête est valide.");
			}
			else
			{
#pragma warning disable CA1304 // Specify CultureInfo
				var parameter = modelState.Keys.FirstOrDefault(key => modelState[key].ValidationState == ModelValidationState.Invalid).ToLower();
#pragma warning restore CA1304 // Specify CultureInfo

				// Log les erreurs
				var badRequestError = GetErrors(modelState);

				throw new BadRequestException(badRequestError);
			}

			return null;
		}

		private BadRequestError GetErrors(ModelStateDictionary modelState)
		{
			var badRequestError = new BadRequestError();

			foreach (var key in modelState.Keys)
			{
				foreach (var modelError in modelState[key].Errors)
				{
					var message = string.IsNullOrEmpty(modelError.ErrorMessage) ? modelError.Exception.Message : modelError.ErrorMessage;
					this.logger.LogWarning($"{SessionId} / {RequestId} - {key} : {message}");
					badRequestError.Errors.Add(new ParameterError(key, message));
				}
			}

			return badRequestError;
		}
	}
}