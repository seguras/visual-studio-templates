﻿namespace WebApiTemplate.Attributes
{
	using System;
	using System.Net;
	using System.Text;
	using Avm.Logging.Abstractions;
	using Avm.Logging.Extensions;
	using Microsoft.AspNetCore.Http;
	using Microsoft.AspNetCore.Mvc;
	using Microsoft.AspNetCore.Mvc.Filters;

	/// <summary>
	/// Gestion des exceptions.
	/// </summary>
	public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
	{
		/// <summary>
		/// Instance du Logger.
		/// </summary>
		private readonly ILogger<CustomExceptionFilterAttribute> logger;

		/// <summary>
		/// Permet l'accès au contexte Http.
		/// </summary>
		private IHttpContextAccessor httpContextAccessor;

		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="CustomExceptionFilterAttribute"/>.
		/// </summary>
		/// <param name="logger">Interface du logger.</param>
		/// <param name="httpContextAccessor">Permet l'accès au contexte Http.</param>
		public CustomExceptionFilterAttribute(ILogger<CustomExceptionFilterAttribute> logger, IHttpContextAccessor httpContextAccessor)
		{
			this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
			this.httpContextAccessor = httpContextAccessor ?? throw new ArgumentNullException(nameof(httpContextAccessor));
		}

		/// <summary>
		/// Obtient l'id de session.
		/// </summary>
		private string SessionId
		{
			get
			{
				return this.httpContextAccessor.HttpContext.Request.Headers["SessionId"];
			}
		}

		/// <summary>
		/// Executé pour chaque exception non catchée.
		/// </summary>
		/// <param name="context">Context de l'exception.</param>
		public override void OnException(ExceptionContext context)
		{
			if (context == null)
			{
				throw new ArgumentNullException(nameof(context));
			}

			context.Result = FormatResult(context.Exception);
			context.ExceptionHandled = true;
		}

		/// <summary>
		/// Format une réponse ActionResult en fonction de l'exception.
		/// </summary>
		/// <param name="exception">Exception à formater.</param>
		/// <returns>Retourne un IactionResult.</returns>
		public virtual IActionResult FormatResult(Exception exception)
		{
			ActionResult actionResult = null;

			switch (exception)
			{
				case ArgumentNullException anex:
					this.logger.LogWarning($"{SessionId} - Exception : " + anex.ToString());
					actionResult = JsonResult(HttpStatusCode.BadRequest, anex);
					break;
				case Exception ex:
					this.logger.LogError($"{SessionId} - Exception : " + ex.ToString());
					actionResult = InternalServerErrorResult(ex.Message);
					break;
			}

			return actionResult;
		}

		/// <summary>
		/// Retourne un 500.
		/// </summary>
		/// <param name="body">information du resultat.</param>
		/// <returns>Retourne un ActionResult.</returns>
		private ActionResult InternalServerErrorResult(string body)
		{
			this.logger.LogDebug($"{SessionId} - Retourne le code HTTP '500'");

			var result = new ContentResult();
			result.StatusCode = (int)HttpStatusCode.InternalServerError;
			result.Content = body;

			return result;
		}

		/// <summary>
		/// Retourne un 503.
		/// </summary>
		/// <returns>Retourne un ActionResult.</returns>
		private ActionResult ServiceUnavailableResult()
		{
			this.logger.LogDebug($"{SessionId} - Retourne le code HTTP '503'");
			return new StatusCodeResult((int)HttpStatusCode.ServiceUnavailable);
		}

		/// <summary>
		/// Retourne un 409 en cas de conflit.
		/// </summary>
		/// <param name="cause">Cause du conflit.</param>
		/// <returns>Retourne un ActionResult.</returns>
		private ActionResult ConflictResult(string cause)
		{
			this.logger.LogDebug($"{SessionId} - Retourne le code HTTP '409'");

			var result = new ContentResult();
			result.StatusCode = (int)HttpStatusCode.Conflict;
			result.Content = cause;

			return result;
		}

		/// <summary>
		/// Retourne un erreur personalisé avec un body json.
		/// </summary>
		/// <param name="statusCode">valeur du code erreur.</param>
		/// <param name="error">Instance de l'exception.</param>
		/// <returns>Retourne un ActionResult.</returns>
		private ActionResult JsonResult(HttpStatusCode statusCode, Exception error)
		{
			this.logger.LogDebug($"{SessionId} - Retourne le code HTTP '{(int)statusCode}'");

			var result = new JsonResult(FormatException(error))
			{
				StatusCode = (int)statusCode,
			};

			return result;
		}

		private string FormatException(Exception error)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(error.Message);

			if (error.InnerException != null)
			{
				sb.Append("<br>");
				sb.Append(FormatException(error.InnerException));
			}

			return sb.ToString();
		}
	}
}