﻿namespace WebApiTemplate.Controllers
{
	using System;
	using Avm.Logging.Abstractions;
	using Avm.Logging.Extensions;
	using Microsoft.AspNetCore.Mvc;
	using WebApiTemplate.Services;

	/// <summary>
	/// Controlleur permettant d'afficher le site web.
	/// </summary>
	public class HomeController : Controller
	{
		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="HomeController"/>.
		/// </summary>
		/// <param name="logger">Instance du logger.</param>
		/// <param name="whiteLabelService">Instance du service de gestion des marques-blanches.</param>
		public HomeController(ILogger<HomeController> logger, IWhiteLabelService whiteLabelService)
		{
			this.Logger = logger ?? throw new ArgumentNullException(nameof(logger));
			this.WhiteLabelService = whiteLabelService ?? throw new ArgumentNullException(nameof(whiteLabelService));
		}

		/// <summary>
		/// Obtient ou définit une instance du logger.
		/// </summary>
		private ILogger<HomeController> Logger { get; set; }

		/// <summary>
		/// Obtient ou définit une instance du service de gestion des marques-blanches.
		/// </summary>
		private IWhiteLabelService WhiteLabelService { get; set; }

		/// <summary>
		/// Retourne la page d'accueil du site web.
		/// </summary>
		/// <returns>La page d'accueil du site web.</returns>
		public IActionResult Index()
		{
			var host = Request.Host.Host;
			Logger.LogInformation($"Demande d'affichage du site pour le Host : {host}");
#if DEBUG
			// En debug : Remplace le host localhost par http://WebApiTemplate-dev.com
			host = "WebApiTemplate-dev.com";
			Logger.LogInformation($"Mode debug --> Utilisation du host : {host}");
#endif

			if (!WhiteLabelService.ExistDomain(host))
			{
				Logger.LogInformation($"Aucun site web n'est défini pour le domaine : {host}");
				return NotFound();
			}

			ViewData["styles"] = WhiteLabelService.GetFileName(host, "styles");
			Logger.LogDebug("Feuille de styles 'styles' : " + ViewData["styles"]);
			ViewData["main"] = WhiteLabelService.GetFileName(host, "main");
			Logger.LogDebug("Script 'main' : " + ViewData["main"]);
			ViewData["polyfills"] = WhiteLabelService.GetFileName(host, "polyfills");
			Logger.LogDebug("Script 'polyfills' : " + ViewData["polyfills"]);
			ViewData["runtime"] = WhiteLabelService.GetFileName(host, "runtime");
			Logger.LogDebug("Script 'runtime' : " + ViewData["runtime"]);

			return View();
		}
	}
}