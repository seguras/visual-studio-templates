﻿namespace WebApiTemplate.Controllers
{
	using System;
	using Avm.Logging.Abstractions;
	using Avm.Logging.Extensions;
	using Microsoft.AspNetCore.Mvc;

	/// <summary>
	/// Classe abtraite de base pour les controller.
	/// </summary>
	public abstract class ApiController : ControllerBase
	{
		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="ApiController"/>.
		/// </summary>
		/// <param name="logger">Interface du logger.</param>
		/// <exception cref="ArgumentNullException">Un ou plusieurs des arguments passés sont null.</exception>
		protected ApiController(ILogger logger)
		{
			Logger = logger ?? throw new ArgumentNullException(nameof(logger));
		}

		/// <summary>
		/// Obtient une instance du logger.
		/// </summary>
		public ILogger Logger { get; private set; }

		/// <summary>
		/// Obtient l'id de session.
		/// </summary>
		protected string SessionId
		{
			get
			{
				return HttpContext.Request.Headers["SessionId"];
			}
		}

		/// <summary>
		/// Obtient l'id de la requête.
		/// </summary>
		protected string RequestId
		{
			get
			{
				return HttpContext.TraceIdentifier;
			}
		}
	}
}