﻿namespace WebApiTemplate.Controllers
{
	using System;
	using System.IdentityModel.Tokens.Jwt;
	using System.Linq;
	using System.Security.Claims;
	using Avm.Logging.Abstractions;
	using Avm.Logging.Extensions;
	using AvmUp.Authentication;
	using Microsoft.AspNetCore.Authorization;
	using Microsoft.AspNetCore.Http;
	using Microsoft.AspNetCore.Mvc;
	using Microsoft.IdentityModel.Tokens;
	using WebApiTemplate.Exceptions;
	using WebApiTemplate.Models;
	using WebApiTemplate.Models.Enums;
	using WebApiTemplate.Services;

	/// <summary>
	/// Controller perméttant d'authentifier un utilisateur.
	/// </summary>
	[ApiController]
	public class AuthController : ApiController
	{
		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="AuthController"/>.
		/// </summary>
		/// <param name="logger">Instance du logger.</param>
		/// <param name="authenticator">Module d'authentification.</param>
		/// <param name="authService">Instance du service d'authentification.</param>
		public AuthController(ILogger<AuthController> logger, IAuthenticator authenticator, IAuthService authService)
			: base(logger)
		{
			Authenticator = authenticator ?? throw new ArgumentNullException(nameof(authenticator));
			AuthService = authService ?? throw new ArgumentNullException(nameof(authService));
		}

		/// <summary>
		/// Obtient ou définit le module d'authentification.
		/// </summary>
		private IAuthenticator Authenticator { get; set; }

		/// <summary>
		/// Obtient ou définit une instance du service d'authentification.
		/// </summary>
		private IAuthService AuthService { get; set; }

		/// <summary>
		/// Authentifie un utilisateur sur le serveur.
		/// </summary>
		/// <param name="loginInfos">Informations de connexion.</param>
		/// <returns>Le token d'authentification.</returns>
		/// <response code="200">Utilisateur authentifié.</response>
		/// <response code="401">Utilisateur non authentifié.</response>
		/// <response code="500">Une erreur serveur s'est produite.</response>
		/// <response code="501">La configuration du client auquel appartient l'Utilisateur n'existe pas.</response>
		[HttpPost]
		[Route("api/auth/login")]
		[ProducesResponseType(typeof(AuthenticationToken), 200)]
		[ProducesResponseType(401)]
		[ProducesResponseType(500)]
		[ProducesResponseType(501)]
		public IActionResult Login([FromBody]LoginModel loginInfos)
		{
			if (loginInfos == null)
			{
				return BadRequest("Les informations de connexion de l'utilisateur sont manquantes.");
			}

			try
			{
				// Récupération de l'utilisateur
				var user = Authenticator.GetUser(loginInfos.UserName);

				if (!Authenticator.IsAuthenticated(user, loginInfos.Password))
				{
					Logger.LogWarning($"Les informations de connexion de l'utilisateur {user.Email} sont invalides");
					return Unauthorized();
				}

				// Récupération des rôles de l'utilisateur
				var userRoles = Authenticator.GetRoles(user.Id);

				int? clientSessionTimeout = null;

				// Génération des claims de l'utilisateur
				var claims = AuthService.GenerateClaims(loginInfos.UserName, user, userRoles);

				// Génération du token d'authentification.
				var tokenString = AuthService.GenerateToken(claims, clientSessionTimeout);

				return Ok(new AuthenticationToken() { Token = tokenString });
			}
			catch (UserNotFoundException unfe)
			{
				Logger.LogException(unfe);
				return Unauthorized();
			}
#pragma warning disable CA1031 // Do not catch general exception types : Il s'agit du catch général
			catch (Exception ex)
#pragma warning restore CA1031 // Do not catch general exception types
			{
				Logger.LogException(ex);
				return StatusCode(500);
			}
		}

		/// <summary>
		/// Raffraichie un token d'identification sur le point d'expirer par un nouveau avec une durée de vie remise à zéro.
		/// </summary>
		/// <param name="refreshInfos">Informations sur le token d'identification sur le point d'expirer.</param>
		/// <returns>Un token d'identification JWT.</returns>
		/// <response code="200">Le token d'authentification de l'utilisateur a été raffraichie.</response>
		/// <response code="400">Les informations sur le token d'identification sont manquantes.</response>
		/// <response code="401">Le token est invalide ou l'algorithme de signature est incorect...</response>
		/// <response code="500">La génération du token est impossible.</response>
		/// <response code="501">La configuration du client auquel appartient l'Utilisateur n'existe pas.</response>
		[HttpPost]
		[Route("api/auth/refreshToken")]
		[ProducesResponseType(typeof(AuthenticationToken), 200)]
		[ProducesResponseType(400)]
		[ProducesResponseType(401)]
		[ProducesResponseType(500)]
		[ProducesResponseType(501)]
		public IActionResult RefreshToken([FromBody]RefreshModel refreshInfos)
		{
			try
			{
				if (refreshInfos == null)
				{
					return BadRequest("Les informations sur le token d'identification sont manquantes");
				}

				var tokenValidationParameters = AuthService.GetTokenValidationParameters();

				var tokenHandler = new JwtSecurityTokenHandler();
				ClaimsPrincipal principal = tokenHandler.ValidateToken(refreshInfos.Token, tokenValidationParameters, out SecurityToken securityToken);

				var jwtSecurityToken = securityToken as JwtSecurityToken;

				if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
				{
					Logger.LogError($"Le token est invalide ou l'algorithme de signature est incorect (attendu : {SecurityAlgorithms.HmacSha256}, trouvé : {jwtSecurityToken.Header.Alg}");
					return Unauthorized();
				}

				var userNameClaim = principal.Claims.SingleOrDefault(x => x.Type == UserClaimType.UserName.ToString());

				if (userNameClaim == null)
				{
					Logger.LogError("Impossible de rafraichir le token car le claim 'UserName' est introuvable.");
					return Unauthorized();
				}

				// Récupération de l'utilisateur
				var user = Authenticator.GetUser(userNameClaim.Value);

				// Récupération des rôles de l'utilisateur
				var userRoles = Authenticator.GetRoles(user.Id);

				int? clientSessionTimeout = null;

				// Génération du token
				string newJwtToken = AuthService.GenerateToken(principal.Claims, clientSessionTimeout);

				return Ok(new AuthenticationToken() { Token = newJwtToken });
			}
			catch (TokenGenerationException tge)
			{
				Logger.LogException(tge);
				return StatusCode(StatusCodes.Status500InternalServerError, "Impossible de génerer le token");
			}
			catch (UserNotFoundException unfe)
			{
				Logger.LogException(unfe);
				return Unauthorized();
			}
			catch (SecurityTokenException ste)
			{
				Logger.LogError($"La validation du token a échouée : {ste.Message}");
				Logger.LogException(ste);
				return Unauthorized();
			}
		}
	}
}