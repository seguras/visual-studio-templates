﻿namespace WebApiTemplate.Settings
{
	using System;
	using System.Collections.Generic;

	/// <summary>
	/// Représente le paramétrage d'une marque blanche.
	/// </summary>
	public class WhiteLabelSettings
	{
		/// <summary>
		/// Contient la liste des bundles Angular pour chaque domaine.
		/// </summary>
		private Dictionary<string, FileMappingSettings> mapping = new Dictionary<string, FileMappingSettings>();

		/// <summary>
		/// Retourne la liste des bundles Angular pour un domaine spécifique.
		/// </summary>
		/// <param name="name">Nom d'un domaine.</param>
		/// <returns>Une instance de FileMappingSettings.</returns>
		public FileMappingSettings this[string name]
		{
			get
			{
				if (mapping.ContainsKey(name))
				{
					return mapping[name];
				}
				else
				{
#pragma warning disable CA1065 // Do not raise exceptions in unexpected locations
					throw new IndexOutOfRangeException($"Le domaine '{name}' n'est pas déclaré.");
#pragma warning restore CA1065 // Do not raise exceptions in unexpected locations
				}
			}

			set
			{
				mapping[name] = value;
			}
		}

		/// <summary>
		/// Retourne le nom du fichier bundle Angular spécifique pour un préfixe et un nom de domaine.
		/// </summary>
		/// <param name="domainName">Nom de domaine.</param>
		/// <param name="filePrefix">Préixe du fichier.</param>
		/// <returns>Le nom du fichier bundle Angular spécifique.</returns>
		public string GetFileName(string domainName, string filePrefix)
		{
			if (mapping.ContainsKey(domainName))
			{
				var fileMappings = mapping[domainName];

				if (fileMappings.ContainsKey(filePrefix))
				{
					return fileMappings[filePrefix];
				}
				else
				{
					throw new IndexOutOfRangeException($"Le fichier 'filePrefix'  n'est pas présent pour le domaine '{domainName}'.");
				}
			}
			else
			{
				throw new IndexOutOfRangeException($"Le domaine '{domainName}' n'est pas déclaré.");
			}
		}

		/// <summary>
		/// Retourne une valeur indiquant s'il existe un paramétrage pour le domaine spécifié.
		/// </summary>
		/// <param name="name">Nom du domaine.</param>
		/// <returns>True, s'il existe un paramétrage pour le domaine; false autrement.</returns>
		public bool ContainsKey(string name)
		{
			return mapping.ContainsKey(name);
		}
	}
}