﻿namespace WebApiTemplate.Settings
{
	/// <summary>
	/// Représente le paramétrage de l'authentification.
	/// </summary>
	public class AuthSettings
	{
		/// <summary>
		/// Obtient ou définit la clé privée utilisée pour générer le jeton d'authentification.
		/// </summary>
		public string PrivateKey { get; set; }

		/// <summary>
		/// Obtient ou définit l'émetteur du token.
		/// </summary>
		public string Issuer { get; set; }

		/// <summary>
		/// Obtient ou définit l'audience du jeton.
		/// </summary>
		public string Audience { get; set; }
	}
}