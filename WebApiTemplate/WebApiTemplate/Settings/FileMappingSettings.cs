﻿namespace WebApiTemplate.Settings
{
	using System.Collections.Generic;

	/// <summary>
	/// Représente les mappages entre un prefixe de fichier et le bundle angular généré correspondant.
	/// </summary>
	public class FileMappingSettings
	{
		/// <summary>
		///  Dictionnaire contenant les mappages entre prefixe de fichier et bundle angular généré correspondant.
		/// </summary>
		private Dictionary<string, string> mapping = new Dictionary<string, string>();

		/// <summary>
		/// Retourne le bundle angular généré correspondant au préfixe spécifié.
		/// </summary>
		/// <param name="prefix">Prefixe du fichier.</param>
		/// <returns>Le bundle angular généré.</returns>
		public string this[string prefix]
		{
			get
			{
				return mapping[prefix];
			}

			set
			{
				mapping[prefix] = value;
			}
		}

		/// <summary>
		/// Retourne une valeur indiquant s'il existe un mappage pour le préfixe spécifié.
		/// </summary>
		/// <param name="prefix">Prefixe du fichier.</param>
		/// <returns>True, s'il existe un mappage pour le préfixe; false autrement.</returns>
		public bool ContainsKey(string prefix)
		{
			return mapping.ContainsKey(prefix);
		}
	}
}