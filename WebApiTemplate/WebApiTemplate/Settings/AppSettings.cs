﻿namespace WebApiTemplate.Settings
{
	using System.ComponentModel.DataAnnotations;

	/// <summary>
	/// Classe des paramètres de l'API.
	/// </summary>
	public class AppSettings
	{
		/// <summary>
		/// Obtient ou définit la partie des paramètres d'authentification du fichier de config.
		/// </summary>
		[Required(ErrorMessage = "La section AuthSettings est nécessaire")]
		public AuthSettings AuthSettings { get; set; }
	}
}