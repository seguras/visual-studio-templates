﻿namespace WebApiTemplate
{
	using System;
	using System.ComponentModel.DataAnnotations;
	using System.IO;
	using Avm.Logging.Extensions;
	using Microsoft.AspNetCore.Hosting;
	using Microsoft.Extensions.DependencyInjection;
	using Microsoft.Extensions.Logging;
	using Microsoft.Extensions.Options;
	using WebApiTemplate.Settings;

#pragma warning disable CA1052 // Static holder types should be Static or NotInheritable
	/// <summary>
	/// Point d'entrée de la Web API.
	/// </summary>
	public class Program
	{
		/// <summary>
		/// Méthode principale.
		/// </summary>
		/// <param name="args">Arguments du programme.</param>
		public static void Main(string[] args)
		{
			var host = GetWebHost();

			var logger = host.Services.GetRequiredService<Avm.Logging.Abstractions.ILogger<Startup>>();
			logger.OperativeInformation($"STARTED WebApiTemplate v{System.Reflection.Assembly.GetEntryAssembly().GetName().Version}");

			CheckSettings(host, logger);

			host.Run();
		}

		/// <summary>
		/// Retourne un hôte web configuré.
		/// </summary>
		/// <returns>L'hôte web configuré.</returns>
		public static IWebHost GetWebHost()
		{
			var webHost = new WebHostBuilder()
				.UseKestrel()
				.UseContentRoot(Directory.GetCurrentDirectory())
				.UseIISIntegration()
				.UseStartup<Startup>()
				.ConfigureLogging(l =>
				{
					l.AddFilter("Microsoft", LogLevel.None);
					l.AddFilter("System", LogLevel.None);
				})
				.Build();

			return webHost;
		}

		/// <summary>
		/// Vérifier que le fichier de configuration est valide.
		/// </summary>
		/// <param name="host">Instance de l'hôte web.</param>
		/// <param name="logger">Instance du logger.</param>
		private static void CheckSettings(IWebHost host, Avm.Logging.Abstractions.ILogger logger)
		{
			try
			{
				var settings = ((IOptions<AppSettings>)host.Services.GetService(typeof(IOptions<AppSettings>))).Value;
				Validator.ValidateObject(settings, new ValidationContext(settings, null, null), true);
			}
			catch (ValidationException ve)
			{
				logger.OperativeError($"Le fichier de config Appsettings.json n'est pas valide - {ve.Message}");
			}
			catch (ArgumentNullException ane)
			{
				logger.OperativeError($"Erreur de lecture du fichier de config Appsettings.json - {ane.Message}");
			}
		}
	}
}