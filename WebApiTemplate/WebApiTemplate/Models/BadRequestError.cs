﻿namespace WebApiTemplate.Models
{
	using System.Collections.Generic;
	using System.Text;

	/// <summary>
	/// Représente les erreurs liées à un retour HTTP 400.
	/// </summary>
	public class BadRequestError
	{
		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="BadRequestError"/>.
		/// </summary>
		public BadRequestError()
		{
			Errors = new List<ParameterError>();
		}

		/// <summary>
		/// Obtient la liste des paramètres en erreur de la requête HTTP.
		/// </summary>
		public List<ParameterError> Errors { get; }

		/// <summary>
		/// Retourne une représentation textuelle de l'objet.
		/// </summary>
		/// <returns>Une représentation textuelle de l'objet.</returns>
		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("[");

			foreach (var error in Errors)
			{
				sb.Append(error.ToString());
				sb.Append(",");
			}

			sb.Append("]");

			return sb.ToString();
		}
	}
}