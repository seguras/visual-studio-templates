﻿namespace WebApiTemplate.Models
{
	/// <summary>
	/// Contient les informations sur un paramètre en erreur d'une requête.
	/// </summary>
	public class ParameterError
	{
		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="ParameterError"/>.
		/// </summary>
		/// <param name="parameterName">Nom du paramètre.</param>
		/// <param name="errorMessage">Message d'erreur.</param>
		public ParameterError(string parameterName, string errorMessage)
		{
			this.ParameterName = parameterName;
			this.ErrorMessage = errorMessage;
		}

		/// <summary>
		/// Obtient ou définit le nom du paramètre.
		/// </summary>
		public string ParameterName { get; set; }

		/// <summary>
		/// Obtient ou définit le message d'erreur.
		/// </summary>
		public string ErrorMessage { get; set; }

		/// <summary>
		/// Retourne une représentation textuelle de l'objet.
		/// </summary>
		/// <returns>Une représentation textuelle de l'objet.</returns>
		public override string ToString()
		{
			return $"Erreur de paramètre (Name: {ParameterName}, Message: {ErrorMessage}";
		}
	}
}