﻿namespace WebApiTemplate.Models
{
	using System.ComponentModel.DataAnnotations;

	/// <summary>
	/// Représente les informations de connexion d'un utilisateur.
	/// </summary>
	public class LoginModel
	{
		/// <summary>
		/// Obtient ou définit l'identifiant de connexion de l'utilisateur.
		/// </summary>
		[Required]
		public string UserName { get; set; }

		/// <summary>
		/// Obtient ou définit le mot de passe de l'utilisateur.
		/// </summary>
		[Required]
		public string Password { get; set; }
	}
}