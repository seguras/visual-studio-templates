﻿namespace WebApiTemplate.Models
{
	using System.ComponentModel.DataAnnotations;

	/// <summary>
	/// Représente les données necessaires au raffraichissement d'un token sur le point d'expirer.
	/// </summary>
	public class RefreshModel
	{
		/// <summary>
		/// Obtient ou définit le token sur le point d'expirer.
		/// </summary>
		[Required]
		public string Token { get; set; }
	}
}