﻿namespace WebApiTemplate.Models.Enums
{
	/// <summary>
	/// Type de revendications de l'utilisateur.
	/// </summary>
	public enum UserClaimType
	{
		/// <summary>
		/// Identifiant de connexion de l'utilisateur..
		/// </summary>
		UserName,

		/// <summary>
		/// Identifiant du client auquel appartient l'utilisateur.
		/// </summary>
		ClientId,

		/// <summary>
		/// Identifiant de l'utilisateur.
		/// </summary>
		UserId,

		/// <summary>
		/// Prénom de l'utilisateur.
		/// </summary>
		FirstName,

		/// <summary>
		/// Nom de l'utilisateur.
		/// </summary>
		LastName,

		/// <summary>
		/// Email de l'utilisateur.
		/// </summary>
		Email,

		/// <summary>
		/// Rôles de l'utilisateur.
		/// </summary>
		Roles,
	}
}