﻿namespace WebApiTemplate.Models
{
	/// <summary>
	/// Représente un token d'authentification.
	/// </summary>
	public class AuthenticationToken
	{
		/// <summary>
		/// Obtient ou définit la valeur du token d'authentification.
		/// </summary>
		public string Token { get; set; }
	}
}