﻿namespace WebApiTemplate.Exceptions
{
	using System;

#pragma warning disable CA1032 // Implement standard exception constructors
	/// <summary>
	/// Levée lorsque la génération des claims d'un utilisateur échoue.
	/// </summary>
	public class ClaimsGenerationException : Exception
	{
		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="ClaimsGenerationException"/>.
		/// </summary>
		/// <param name="message">message d'erreur.</param>
		public ClaimsGenerationException(string message)
			: base($"Impossible de génerer les claims de l'utilisateur : {message}")
		{
		}

		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="ClaimsGenerationException"/>.
		/// </summary>
		/// <param name="innerException">Exception d'origine.</param>
		public ClaimsGenerationException(Exception innerException)
			: base($"Impossible de génerer les claims de l'utilisateur", innerException)
		{
		}
	}
}