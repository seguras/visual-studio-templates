﻿namespace WebApiTemplate.Exceptions
{
	using System;

#pragma warning disable CA1032 // Implement standard exception constructors
	/// <summary>
	/// Levée lorsque la génération d'un token échoue.
	/// </summary>
	public class TokenGenerationException : Exception
	{
		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="TokenGenerationException"/>.
		/// </summary>
		/// <param name="message">message d'erreur.</param>
		public TokenGenerationException(string message)
			: base($"Impossible de génerer le token : {message}")
		{
		}

		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="TokenGenerationException"/>.
		/// </summary>
		/// <param name="innerException">Exception d'origine.</param>
		public TokenGenerationException(Exception innerException)
			: base($"Impossible de génerer le token", innerException)
		{
		}
	}
}