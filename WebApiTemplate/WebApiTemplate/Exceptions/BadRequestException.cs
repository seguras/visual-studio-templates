﻿namespace WebApiTemplate.Exceptions
{
	using System;
	using WebApiTemplate.Models;

	/// <summary>
	/// Exception levée lorsqu'un ou plusieurs paramètres d'une requête sont invalides.
	/// </summary>
#pragma warning disable CA1032 // Implement standard exception constructors
	public class BadRequestException : Exception
	{
		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="BadRequestException"/>.
		/// </summary>
		/// <param name="error">Objet contenant la liste des paramètres en erreur.</param>
		public BadRequestException(BadRequestError error)
		{
			this.Error = error;
		}

		/// <summary>
		/// Obtient un objet contenant la liste des paramètres en erreur.
		/// </summary>
		public BadRequestError Error { get; }
	}
}