﻿namespace WebApiTemplate.Services
{
	/// <summary>
	/// Interface du service de gestion des marques-blanches.
	/// </summary>
	public interface IWhiteLabelService
	{
		/// <summary>
		/// Retourne le nom du fichier correpondant au domaine et au préfixe spécifiés.
		/// </summary>
		/// <param name="domainName">Nom du domaine.</param>
		/// <param name="filePrefix">Préfixe du fichier.</param>
		/// <returns>Le nom du fichier.</returns>
		string GetFileName(string domainName, string filePrefix);

		/// <summary>
		/// Retourne une valeur indiquant si une configuration spécifique existe pour un domaine donné.
		/// </summary>
		/// <remarks>Le systeme determine si une configuration existe en vérifiant la présence d'un dossier dont le nom
		/// correspond au domaine dans le dossier static\whiteLables.</remarks>
		/// <param name="domainName">Nom du domaine.</param>
		/// <returns>True s'il existe une configuration spécifique; false autrement.</returns>
		bool ExistDomain(string domainName);
	}
}