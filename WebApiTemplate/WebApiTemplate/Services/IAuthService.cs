﻿namespace WebApiTemplate.Services
{
	using System;
	using System.Collections.Generic;
	using System.Security.Claims;
	using AvmUp.Authentication;
	using Microsoft.IdentityModel.Tokens;
	using WebApiTemplate.Exceptions;

	/// <summary>
	/// Interface du service utilisé pour l'authentification.
	/// </summary>
	public interface IAuthService
	{
		/// <summary>
		/// Retourne les paramètres de validation d'un token.
		/// </summary>
		/// <returns>Les paramètres de validation d'un token.</returns>
		TokenValidationParameters GetTokenValidationParameters();

		/// <summary>
		/// Génère un token d'identification JWT.
		/// </summary>
		/// <param name="claims">Claims a ajouté au token.</param>
		/// <param name="clientSessionTimeout">Timeout de la session client.</param>
		/// <returns>Un token d'identification JWT.</returns>
		/// <exception cref="TokenGenerationException">Si la génération du token échoue.</exception>
		string GenerateToken(IEnumerable<Claim> claims, int? clientSessionTimeout);

		/// <summary>
		/// Génère les claims d'un utilisateur.
		/// </summary>
		/// <param name="userName">identifiant de connexion de l'utilisateur.</param>
		/// <param name="user">Informations de l'utilisateur.</param>
		/// <param name="roles">Roles de l'utilisateur.</param>
		/// <returns>La liste des claims de l'utilisateur.</returns>
		/// <exception cref="ArgumentNullException">Si un des paramètres n'est pas renseignés.</exception>
		/// <exception cref="ClaimsGenerationException">Si la génération des claims a échouée.</exception>
		IList<Claim> GenerateClaims(string userName, User user, IEnumerable<string> roles);
	}
}