﻿namespace WebApiTemplate.Services.Implementations
{
	using System;
	using System.IO;
	using System.Text.RegularExpressions;
	using Avm.Logging.Abstractions;
	using Avm.Logging.Extensions;
	using Microsoft.AspNetCore.Hosting;
	using WebApiTemplate.Settings;

	/// <summary>
	/// Implémentation du service de gestion des marques-blanches.
	/// </summary>
	public class WhiteLabelService : IWhiteLabelService
	{
		/// <summary>
		/// Pattern des fichiers bundles générés par Angular.
		/// </summary>
		private string filePattern = @"(?<prefix>\w*)\.(?<guid>\w+)(\.)?(js|css)";

		/// <summary>
		/// Paramétrage de la marque blanche.
		/// </summary>
		private WhiteLabelSettings whiteLabelSettings = null;

		/// <summary>
		/// Instance du logger.
		/// </summary>
		private ILogger<WhiteLabelService> logger;

		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="WhiteLabelService"/>.
		/// </summary>
		/// <param name="logger">Instance du logger.</param>
		/// <param name="env">Informations sur l'environnement.</param>
		public WhiteLabelService(ILogger<WhiteLabelService> logger, IHostingEnvironment env)
		{
			this.logger = logger ?? throw new ArgumentNullException(nameof(logger));

			if (env == null)
			{
				throw new ArgumentNullException(nameof(env));
			}

			Regex fileRegex = new Regex(filePattern);

			whiteLabelSettings = new WhiteLabelSettings();

			var whiteLabelsRootPath = Path.Combine(env.ContentRootPath, "static/whiteLabels");

			this.logger.LogInformation($"Recheche des domaines à partir de {whiteLabelsRootPath}...");

			foreach (var directoryPath in Directory.GetDirectories(whiteLabelsRootPath))
			{
				var domainName = Path.GetFileName(directoryPath);
				this.logger.LogInformation($"Domaine trouvé : {domainName}");

				whiteLabelSettings[domainName] = new FileMappingSettings();

				var fileNames = Directory.GetFileSystemEntries(directoryPath);

				this.logger.LogInformation($"Liste des bundles du domaine : {domainName} : ");
				foreach (var fileName in fileNames)
				{
					var matchFile = fileRegex.Match(fileName);

					if (matchFile.Success)
					{
						whiteLabelSettings[domainName][matchFile.Groups["prefix"].Value] = matchFile.Value;
						this.logger.LogInformation($"   {matchFile.Groups["prefix"].Value} : {matchFile.Value} ");
					}
				}
			}
		}

		/// <summary>
		/// Retourne le nom du fichier correpondant au domaine et au préfixe spécifiés.
		/// </summary>
		/// <param name="domainName">Nom du domaine.</param>
		/// <param name="filePrefix">Préfixe du fichier.</param>
		/// <returns>Le nom du fichier.</returns>
		public string GetFileName(string domainName, string filePrefix)
		{
			return Path.Combine("whiteLabels", domainName, whiteLabelSettings[domainName][filePrefix]);
		}

		/// <summary>
		/// Retourne une valeur indiquant si une configuration spécifique existe pour un domaine donné.
		/// </summary>
		/// <remarks>Le systeme determine si une configuration existe en vérifiant la présence d'un dossier dont le nom
		/// correspond au domaine dans le dossier static\whiteLables.</remarks>
		/// <param name="domainName">Nom du domaine.</param>
		/// <returns>True s'il existe une configuration spécifique; false autrement.</returns>
		public bool ExistDomain(string domainName)
		{
			return whiteLabelSettings.ContainsKey(domainName);
		}
	}
}