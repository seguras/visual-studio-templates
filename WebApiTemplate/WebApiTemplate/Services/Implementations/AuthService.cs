﻿namespace WebApiTemplate.Services.Implementations
{
	using System;
	using System.Collections.Generic;
	using System.IdentityModel.Tokens.Jwt;
	using System.Security.Claims;
	using System.Text;
	using AvmUp.Authentication;
	using Microsoft.Extensions.Options;
	using Microsoft.IdentityModel.Tokens;
	using WebApiTemplate.Exceptions;
	using WebApiTemplate.Models.Enums;
	using WebApiTemplate.Settings;

	/// <summary>
	/// Implémentation du service utilisé pour l'authentification.
	/// </summary>
	public class AuthService : IAuthService
	{
		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="AuthService"/>.
		/// </summary>
		/// <param name="settings">Paramétrage lié à l'authentification.</param>
		public AuthService(IOptions<AuthSettings> settings)
		{
			this.Settings = settings != null ? settings.Value : throw new ArgumentNullException(nameof(settings));

			SecretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.Settings.PrivateKey));
			SigningCredentials = new SigningCredentials(SecretKey, SecurityAlgorithms.HmacSha256);
		}

		/// <summary>
		/// Obtient ou définit le paramétrage lié à l'authentification.
		/// </summary>
		private AuthSettings Settings { get; set; }

		/// <summary>
		/// Obtient ou définit la clé secrète utilisée pour signer le token.
		/// </summary>
		private SymmetricSecurityKey SecretKey { get; set; }

		/// <summary>
		/// Obtient ou définit une instance d'objet spécifiant la clé de signature et l'algorithme de sécurité utilisé.
		/// </summary>
		private SigningCredentials SigningCredentials { get; set; }

		/// <summary>
		/// Retourne les paramètres de validation d'un token.
		/// </summary>
		/// <returns>Les paramètres de validation d'un token.</returns>
		public TokenValidationParameters GetTokenValidationParameters()
		{
			var tokenValidationParameters = new TokenValidationParameters
			{
				ValidateIssuer = true,
				ValidateAudience = true,
				ValidateLifetime = true,
				ValidateIssuerSigningKey = true,

				ValidIssuer = this.Settings.Issuer,
				ValidAudience = this.Settings.Audience,
				IssuerSigningKey = SecretKey,
			};

			return tokenValidationParameters;
		}

		/// <summary>
		/// Génère un token d'identification JWT.
		/// </summary>
		/// <param name="claims">Claims a ajouté au token.</param>
		/// <param name="clientSessionTimeout">Timeout de la session client.</param>
		/// <returns>Un token d'identification JWT.</returns>
		/// <exception cref="TokenGenerationException">Si la génération du token échoue.</exception>
		public string GenerateToken(IEnumerable<Claim> claims, int? clientSessionTimeout)
		{
			if (claims == null)
			{
				throw new ArgumentNullException(nameof(claims));
			}

			try
			{
				var jwt = new JwtSecurityToken(
					issuer: this.Settings.Issuer,
					audience: this.Settings.Audience,
					claims: claims,
					expires: DateTime.Now.AddMinutes(clientSessionTimeout ?? 120),
					signingCredentials: SigningCredentials);

				return new JwtSecurityTokenHandler().WriteToken(jwt);
			}
			catch (Exception ex)
			{
				throw new TokenGenerationException(ex);
			}
		}

		/// <summary>
		/// Génère les claims d'un utilisateur.
		/// </summary>
		/// <param name="userName">identifiant de connexion de l'utilisateur.</param>
		/// <param name="user">Informations de l'utilisateur.</param>
		/// <param name="roles">Roles de l'utilisateur.</param>
		/// <returns>La liste des claims de l'utilisateur.</returns>
		/// <exception cref="ArgumentNullException">Si un des paramètres n'est pas renseignés.</exception>
		/// <exception cref="ClaimsGenerationException">Si la génération des claims a échouée.</exception>
		public IList<Claim> GenerateClaims(string userName, User user, IEnumerable<string> roles)
		{
			if (string.IsNullOrEmpty(userName))
			{
				throw new ArgumentNullException(nameof(userName));
			}

			if (user == null)
			{
				throw new ArgumentNullException(nameof(user));
			}

			if (roles == null)
			{
				throw new ArgumentNullException(nameof(roles));
			}

			try
			{
				var claims = new List<Claim>
				{
					new Claim(UserClaimType.UserName.ToString(), userName ?? string.Empty),
					new Claim(UserClaimType.ClientId.ToString(), user.ClientId ?? string.Empty),
					new Claim(UserClaimType.UserId.ToString(), user.Id ?? string.Empty),
					new Claim(UserClaimType.FirstName.ToString(), user.FirstName ?? string.Empty),
					new Claim(UserClaimType.LastName.ToString(), user.LastName ?? string.Empty),
					new Claim(UserClaimType.Email.ToString(), user.Email ?? string.Empty),
				};

				foreach (string role in roles)
				{
					claims.Add(new Claim(ClaimTypes.Role, role)); // Utilisé par la WebApi
					claims.Add(new Claim(UserClaimType.Roles.ToString(), role)); // Utilisé par l'appli Angular
				}

				return claims;
			}
			catch (Exception ex)
			{
				throw new ClaimsGenerationException(ex);
			}
		}
	}
}