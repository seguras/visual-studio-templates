﻿namespace PoolingModule
{
	using System;
	using System.Collections.Generic;
	using System.Text;

	public interface ITreatment
	{
		/// <summary>
		/// Execute le traitement.
		/// </summary>
		void Execute();
	}
}