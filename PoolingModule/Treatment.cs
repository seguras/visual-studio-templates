﻿namespace PoolingModule
{
	using System;
	using Avm.Logging.Abstractions;
	using Avm.Logging.Extensions;
	using Microsoft.Extensions.Options;
	using PoolingModule.Settings;

	public class Treatment : ITreatment
	{
		/// <summary>
		/// logger du traitement.
		/// </summary>
		private ILogger<Treatment> logger;

		/// <summary>
		/// Configuration du service.
		/// </summary>
		private ServiceConfiguration serviceConfiguration;

		/// <summary>
		/// Initialise une nouvelle instance de la classe <see cref="Treatment"/>.
		/// </summary>
		/// <param name="logger">Instance du logger.</param>
		/// <param name="serviceConfiguration">Configuration du service.</param>
		public Treatment(ILogger<Treatment> logger, IOptions<ServiceConfiguration> serviceConfiguration)
		{
			this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
			this.serviceConfiguration = serviceConfiguration != null ? serviceConfiguration.Value : throw new ArgumentNullException(nameof(serviceConfiguration));
		}

		/// <summary>
		/// Execute le traitement.
		/// </summary>
		public void Execute()
		{
			this.logger.LogInformation("Execution du traitement...");
		}
	}
}