﻿namespace PoolingModule
{
	using System;
	using System.IO;
	using System.Runtime.Loader;
	using System.Threading;
	using System.Threading.Tasks;
	using Avm.Logging.Abstractions;
	using Avm.Logging.Extensions;
	using Avm.Logging.NLog.NetCore.Extensions;
	using Avm.Logging.NLogAdapter;
	using Microsoft.Extensions.Configuration;
	using Microsoft.Extensions.DependencyInjection;
	using Microsoft.Extensions.Options;
	using PoolingModule.Settings;

	/// <summary>
	/// programme principal.
	/// </summary>
	public class Program
	{
		/// <summary>
		/// Source du jeton d'annulation pour arrêter la tâche principale.
		/// </summary>
		private static readonly CancellationTokenSource CancelTokenSource = new CancellationTokenSource();

		/// <summary>
		/// Configuration générale.
		/// </summary>
		private static IConfigurationRoot configuration;

		/// <summary>
		/// Injecteur de dépendances.
		/// </summary>
		private static IServiceProvider serviceProvider;

		/// <summary>
		/// Logger de l'application.
		/// </summary>
		private static NLogAdapter<Program> logger = new NLogAdapter<Program>();

		private static DateTime? previousTreatmentExecutionDate = null;

		/// <summary>
		/// Fonction principal.
		/// </summary>
		/// <param name="args">liste d'argument.</param>
		public static void Main(string[] args)
		{
			// lecture de la configuration
			configuration = new ConfigurationBuilder()
							.SetBasePath(Directory.GetCurrentDirectory())
							.AddJsonFile("appsettings.json")
							.Build();

			// injecteur de dépendances
			IServiceCollection services = new ServiceCollection()
							.AddLogging()
							.RegisterAvmLogging()
							.AddOptions();

			ConfigureServices(services);

			serviceProvider = services.BuildServiceProvider();

			var loggerFactory = serviceProvider.GetService<Microsoft.Extensions.Logging.ILoggerFactory>()
				.ConfigureAvmLogging("NLog.config");

			// ajoute méthode de callback si arrêt du service
			AssemblyLoadContext.Default.Unloading += Default_Unloading;

			// attend en asynchrone le traitement principal
			Task task = MainLoop((ITreatment)serviceProvider.GetRequiredService(typeof(ITreatment)), (IOptions<ServiceConfiguration>)serviceProvider.GetRequiredService(typeof(IOptions<ServiceConfiguration>)));
			task.GetAwaiter().GetResult();
		}

		private static void ConfigureServices(IServiceCollection services)
		{
			services.Configure<AppSettings>(options => configuration.GetSection("AppSettings").Bind(options));
			services.Configure<ServiceConfiguration>(options => configuration.GetSection("AppSettings:ServiceConfiguration").Bind(options));

			// Ajouter ici le bindings des sections de configuration du fichier appsettings.json
			services.AddScoped<ITreatment, Treatment>();
		}

		/// <summary>
		/// méthode appelée à l'arrêt du service Linux.
		/// </summary>
		/// <param name="obj">contexte de l'assembly.</param>
		private static void Default_Unloading(AssemblyLoadContext obj)
		{
			CancelTokenSource.Cancel();
			logger.LogInformation("Arrêt du service PoolingModule");
			logger.OperativeInformation("STOPPED PoolingModule");
		}

		/// <summary>
		/// boucle principale de traitement.
		/// </summary>
		/// <returns>objet Task awaitable.</returns>
		private static async Task MainLoop(ITreatment treatment, IOptions<ServiceConfiguration> serviceConfiguration)
		{
			logger.LogInformation("Démarrage du service PoolingModule");
			logger.OperativeInformation("STARTED PoolingModule");

			int delayBeetweenTreatmentExecution = serviceConfiguration.Value.DelayBeetweenTreatmentExecutionInSeconds;

			while (!CancelTokenSource.Token.IsCancellationRequested)
			{
				try
				{
					previousTreatmentExecutionDate = DateTime.Now;
					treatment.Execute();
				}
				catch (Exception e)
				{
					logger.OperativeError($"{e}");
				}

				var executionTime = (DateTime.Now - previousTreatmentExecutionDate.Value).TotalSeconds;

				logger.LogDebug($"Traitement éxécuté en {executionTime} secondes.");

				if (executionTime < delayBeetweenTreatmentExecution)
				{
					try
					{
						// Pause jusqu'au tour suivant
						logger.LogDebug($"Attente de {delayBeetweenTreatmentExecution - executionTime} secondes avant la prochaine exécution...");
						await Task.Delay(TimeSpan.FromSeconds(serviceConfiguration.Value.DelayBeetweenTreatmentExecutionInSeconds), CancelTokenSource.Token);
					}
					catch (TaskCanceledException)
					{
						// Une demande d'arrêt du service a été reçue.
					}
				}
			}
		}
	}
}