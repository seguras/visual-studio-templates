﻿namespace PoolingModule.Settings
{
	using System.ComponentModel.DataAnnotations;

	/// <summary>
	/// ReprÃ©sente la configuration stockÃ©e dans le fichier appsettings.json.
	/// </summary>
	public class AppSettings
	{
		/// <summary>
		/// Obtient ou définit la section ServiceConfiguration du fichier de config.
		/// </summary>
		[Required(ErrorMessage = "La section ServiceConfiguration est nÃ©cessaire")]
		public ServiceConfiguration ServiceConfiguration { get; set; }
	}
}