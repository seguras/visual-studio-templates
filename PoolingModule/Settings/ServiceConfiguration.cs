﻿namespace PoolingModule.Settings
{
	using System.ComponentModel.DataAnnotations;

	/// <summary>
	/// Configuration du service.
	/// </summary>
	public class ServiceConfiguration
	{
		/// <summary>
		/// Obtient ou définit le délai minimum entre deux execution du traitement (Le délai est exprimé en secondes).
		/// </summary>
		/// <remarks>Si le temps d'execution du traitement est inférieur à cette valeur, alors le programme se mettra en attente.
		/// Le programme se reveillera lorsque le délai spécifié depuis le début de la precedante execution se sera écoulé.
		/// Si le temps d'execution du traitement est supérieur à cette valeur, alors le programme enchaine l'execution du traitement suivant immediatement.
		/// </remarks>
		[Required(ErrorMessage = "La valeur du délai minimum entre deux execution du traitement est manquante")]
		public int DelayBeetweenTreatmentExecutionInSeconds { get; set; }
	}
}